# RetroShield 8031 for Arduino Mega

* RetroShield 801 is a hardware shield for Arduino Mega. 
* A real 8031 execute code while Arduino emulates RAM/ROM and peripherals.
* Arduino Mega gives you 4~6KB of RAM and >200KB ROM (in flash area).
* All hardware and software files are provided for you to study and play.
* Hardware details and PCB/kits for sale: http://www.8bitforce.com/

### 8031 Links

* Find asm51.exe online to compile code.  I can send it to you if you want.
* SB-Assembler: https://www.sbprojects.net/sbasm/
